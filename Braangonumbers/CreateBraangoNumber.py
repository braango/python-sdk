from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint

'''
  @author braango
 
     Sample code showing how to create
     one braango number for subdealer
     for given group
 
 '''

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you

braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'


# create an instance of the API class
api_instance = braango.BraangonumbersApi()

# str | 
group = 'DEFAULT' 

# str | id of _sub_dealer_
subdealerid = 'subdealers2002'


api_Key =  'ISNMdzuNiKG7jhl9d9v'

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

idBraangoNumber = "create-braango-number"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,idBraangoNumber,account_type)


#All all the personnel in represented
#by this group for this sub-dealer

all_personnel = True

fake_braango_number = True

# { all_personnel, fake_braango_number, sales_person_id_multi}
braangoNumberCreateInputBody = braango.models.BraangoNumberCreateInputBody(all_personnel,fake_braango_number,None)

braangoNumberCreateInput = braango.BraangoNumberCreateInput(requestHeader, braangoNumberCreateInputBody) # BraangoNumberCreateInput |  (optional)

try: 
    # Create braangonumber
    api_response = api_instance.create_braango_number(subdealerid, group, body=braangoNumberCreateInput)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BraangonumbersApi->create_braango_number: %s\n" % e)
 
 