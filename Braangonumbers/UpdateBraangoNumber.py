from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint

'''
 
  @author braango
         
          Sample code to update braango
          numbers for subDealer
         
'''   

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.BraangonumbersApi()

# str | subdealerid
subdealerid = 'subdealers2004'

# str | braangonumber_or_uuid_example
braangonumber_or_uuid =  'f652c0d4-180d-48a0-9edf-e8c2efaef9de'

api_Key =  'ISNMdzuNiKG7jhl9d9v'

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

idBraangoNumber = "Update-braango-number"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,idBraangoNumber,account_type)

all_personnel = True

fake_braango_number = True

add = False

sales_person_id_multi = ["4fd2259e-10cb-46ce-b301-cf0b17677282"]

# {all_personnel, fake_braango_number, add, sales_person_id_multi}
braangoNumberInputBody = braango.models.BraangoNumberInputBody(all_personnel, fake_braango_number, add, sales_person_id_multi)

# BraangoNumberInput |
body = braango.BraangoNumberInput(requestHeader,braangoNumberInputBody)

try: 
    # Update braangonumber
    api_response = api_instance.update_braango_number(subdealerid, braangonumber_or_uuid, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BraangonumbersApi->update_braango_number: %s\n" % e)      
         
        
 