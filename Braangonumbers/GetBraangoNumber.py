from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint


'''
  @author braango
         
          Sample code showing how to get one braango number
          for sub dealer
'''   

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'


# create an instance of the API class
api_instance = braango.BraangonumbersApi()

# str | subdealerid
subdealerid = 'subdealers2005'

# str | braangonumber_or_uuid_example
braangonumber_or_uuid =  '2434faae-9f28-4de6-804d-370f8286965f'

# str | API Key to access this dealer's resources. 
#Value was returned when create_account api was called and 
#dealer was created first time 
api_key = 'ISNMdzuNiKG7jhl9d9v'

# str | Dealer or partner is accessing this API
account_type = 'partner'

try: 
    # Get braangonumber
    api_response = api_instance.get_braango_number(subdealerid, braangonumber_or_uuid, api_key, account_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BraangonumbersApi->get_braango_number: %s\n" % e)     
         
         
 