from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint


'''
  @author braango
         
          Sample code showing how to get all braango
          numbers for subDealer
'''
#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] =  'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.BraangonumbersApi()

# str | subdealerid
subdealerid = 'subdealers2004'

# str | API Key to access this dealer's resources. 
#Value was returned when create_account api was called and 
#dealer was created first time 
api_key = 'ISNMdzuNiKG7jhl9d9v'

# str | Dealer or partner is accessing this API
account_type = 'partner'

try: 
    # List braangonumbers
    api_response = api_instance.get_braango_numbers(subdealerid, api_key, account_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BraangonumbersApi->get_braango_numbers: %s\n" % e)
         
         
         
 
