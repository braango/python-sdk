from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint

''' 
  @author braango
         
          Sample code showing how to blast
          to multiple consumers via
          partner's blast number (different than
          braango number)
'''

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you  
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.MessagingApi()

 # str | subdealer_id
subdealerid = 'subdealers2004'

api_Key ="ISNMdzuNiKG7jhl9d9v"

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts
account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response
id_messageBlast = "create-personnel-s2004r1"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,id_messageBlast,account_type)

phone_number = "6692459240"

number_id = "required-unique-id"
  
message = ["Line 1"]

# { phone_number, number_id, message, media_urls }
messageBlastBody = braango.models.MessageBlastNumbers(phone_number, number_id, message, None)

messageBlast = braango.models.MessageBlast([messageBlastBody])

# MessageBlastRequestInput |  
messageBlastRequestInput = braango.MessageBlastRequestInput(requestHeader, messageBlast) 

try: 
    # Message Blast
    api_response = api_instance.message_blast(body=messageBlastRequestInput)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MessagingApi->message_blast: %s\n" % e)       
         
 
