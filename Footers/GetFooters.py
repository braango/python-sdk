from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint


''' 
  @author braango
         
         Sample code showing how to get Footers for a personnel
'''  
#TEST auth token. Please contact
#sales@braango.com to have one
#created for you 

braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'


# create an instance of the API class
api_instance = braango.FootersApi()

# str | id of _sub_dealer_
subdealerid = 'subdealers2004' 

# str | id of _personnel_
salespersonid = 'c5313913-5768-4eef-b5ea-447cc8f5aec3' 

# str | Banner type - `client` , `dealer` , `supervisor`
footertype = 'client' 

# str | API Key to access this dealer's resources. 
#Value was returned when create_account api was called and 
#dealer was created first time 
api_key = 'ISNMdzuNiKG7jhl9d9v'

# str | Dealer or partner is accessing this API
account_type = 'partner'


try: 
    # Get Footers
    api_response = api_instance.get_footers(subdealerid, salespersonid, footertype, api_key, account_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FootersApi->get_footers: %s\n" % e)      