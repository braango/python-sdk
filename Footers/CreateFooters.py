from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint

'''
 
  @author braango
         
          Sample code showing how to add Footers for a personnel
'''
#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] =  'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.FootersApi()

# str | id of _sub_dealer_
subdealerid = 'subdealers2004'

# str | id of _personnel_
salespersonid = 'c5313913-5768-4eef-b5ea-447cc8f5aec3' 

api_Key =  'ISNMdzuNiKG7jhl9d9v'

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

idBanner = "footers-create-s2004"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,idBanner,account_type)

dealer_footers = ["s2004df1-dealer api"]

client_footers = ["s2004cf1-client api"]

supervisor_footers = ["s2004sf1-supervisor api"]

# { dealer_footers, client_footers, supervisor_footers }

footersInputBody = braango.models.FootersInputBody(dealer_footers, client_footers, supervisor_footers)

# FootersInput |  
footersInput = braango.FootersInput(requestHeader,footersInputBody)

try: 
    # Create Footers
    api_response = api_instance.create_footers(subdealerid, salespersonid, body=footersInput)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FootersApi->create_footers: %s\n" % e)

