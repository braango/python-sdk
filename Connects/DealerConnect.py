from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint


''' 
  @author braango
         
          Sample code showing how to use
          dealerConnect api to enable
          bridge between seeded client
          and the sub-dealer
'''

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.ConnectsApi()

# str | id of _sub_dealer_
subdealerid = 'subdealers2004' 

# str | id of _personnel_
salespersonid = 'c5313913-5768-4eef-b5ea-447cc8f5aec3' 

api_Key ="ISNMdzuNiKG7jhl9d9v"

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

id_dealer = "add-delaer-04"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,id_dealer,account_type)

client_number = "6692459240"

connect_enable =  True

braango_number = "555-555-5555"

message = ["Line 1"]

# { client_number, connect_enable, connect_id, braango_number, message, media_urls, send_message, send_message_only}
dealerConnectBody = braango.models.DealerConnect(client_number, connect_enable, None, braango_number, message, None)

# DealerConnectRequestInput |  
dealerConnectRequestInput = braango.DealerConnectRequestInput(requestHeader,dealerConnectBody) 

try: 
    # Dealer Connect
    api_instance.dealer_connect(subdealerid, salespersonid, body= dealerConnectRequestInput)
except ApiException as e:
    print("Exception when calling ConnectsApi->dealer_connect: %s\n" % e)