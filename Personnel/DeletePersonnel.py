from __future__ import print_function
import time
import braango
from braango.rest import ApiException
from pprint import pprint

'''

 
  @author braango
         
          Sample code to delete personnel details
         
          Deleting personnel wipes out all the entries from the system
          Though user name cannot be deleted for security reasons
         
 
'''

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.PersonnelsApi()

# str | Sub dealer for which this sales person belongs to.
subdealerid = 'subdealers2002' 

# str | Sales person ID that was returned when this personnel was created
salespersonid = 'a41c39d4-6f7f-40be-964d-8431081ea4d2' 

# str | API Key to access this dealer's resources. 
#Value was returned when create_account api was 
#called and dealer was created first time 
api_key = 'ISNMdzuNiKG7jhl9d9v' 

# str | Dealer or partner is accessing this API 
account_type = 'partner' 

try: 
    # Delete personnel
    api_response = api_instance.delete_personnel(subdealerid, salespersonid, api_key, account_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PersonnelsApi->delete_personnel: %s\n" % e)
