from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint
from braango.models.personnel_update_request_input import PersonnelUpdateRequestInput

 
 #TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.PersonnelsApi()

 # str | Sub dealer for which this sales person belongs to.
subdealerid = 'subdealers2004'

# str | Sales person ID that was returned when this personnel was created
salespersonid = '4fd2259e-10cb-46ce-b301-cf0b17677282' 

api_Key ="ISNMdzuNiKG7jhl9d9v"

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

id_personnel = "update-personnel-s2004r1"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,id_personnel,account_type)

#Required field . Used for
#SMS login in to the UI
#For Braango Enterprise, this is don't care
#unless partner implements UI with SMS login
sms_login = False


#Password will be encrypted with SHA-25 and base64 encoded and stored
#internally within the braango system. pattern:
#^(?=^.{6,10}$)(?=.*\d)(
#?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+}{&quot
#;:;'?/>.<,])(?!.*\s).*$
#Used for single sign on. needs to 6-10 characters with one capital,
#one numberal and one special character

password = "test1T$"

#Required
email = "s2004@subdealer2004.com"

#If email specified is that for
#CRM email (ADF XML compliant)
type_adf_crm = False


#Number where dealer's root account can be reached via SMS for leads
#pattern:^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$
phone_number = "4089763493"

#Number where dealer's root account can be reached via SMS for leads
#pattern:^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$
sms_number = "4089763493"

#Subscribe to the group
group = "s2004g"

#List of dealer banners. Braango will
#randomly choose one when sending
#message to dealer via SMS
dealer_banners = ["s2004r1db1updated"];


#List of client banners. Braango
#will randomly choose one when
#sending dealer messages to client
client_banners = ["s2004r1cb1updated"]


#List of dealer footers. Braango will
#randomly choose one when sending
#message to dealer via SMS
dealer_footers = ["s2004r1df1updated"];

#List of client footers. Braango
#will randomly choose one when
#sending dealer messages to client
client_footers = ["s2004r1cf1updated"];

#List of supervisor banners. Braango
#will randomly choose one when
#sending messages to supervisor
supervisor_banners = ["s2004r1sb1updated"];


#List of supervisor footers. Braango
#will randomly choose one when
#sending messages to supervisor
supervisor_footers = ["s2004r1sf1updated"];


# { sms_login, password, status, dealer_banners, client_banners, supervisor_banners, client_footers, dealer_footers, supervisor_footers, 
# sms_number, phone_number, email, type_adf_crm, group }

personnelUpdateBody = braango.models.PersonnelUpdate(sms_login,password,None,dealer_banners,client_banners,supervisor_banners,
                                                     client_footers,dealer_footers,supervisor_footers,sms_number,phone_number,email,type_adf_crm,group)




 # PersonnelUpdateRequestInput 
personnelUpdateRequestInput = braango.PersonnelUpdateRequestInput(requestHeader,personnelUpdateBody)

try: 
    # Update personnel
    api_response = api_instance.update_personnel(subdealerid, salespersonid, body=personnelUpdateRequestInput)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PersonnelsApi->update_personnel: %s\n" % e)