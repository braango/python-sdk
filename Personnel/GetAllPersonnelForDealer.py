from __future__ import print_function
import time
import braango
from braango.rest import ApiException
from pprint import pprint


'''
 
  @author braango
         
          Sample code to fetch all personnel for virtual dealer
'''


#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.PersonnelsApi()

# str | API Key to access this dealer's resources. 
#Value was returned when create_account api was 
#called and dealer was created first time 
api_key = 'ISNMdzuNiKG7jhl9d9v'

# str | Dealer or partner is accessing this API  
account_type = 'partner' 

try: 
    # List all personnels
    api_response = api_instance.get_all_personnel_for_dealer(api_key, account_type)
    print("API called successfully. Returned data: " )
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PersonnelsApi->get_all_personnel_for_dealer: %s\n" % e)