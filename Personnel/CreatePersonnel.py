from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint


'''
  @author braango
        
         Sample code showing how to create subDealer
            
 '''
 
#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'


# create an instance of the API class
api_instance = braango.PersonnelsApi()

 # str | subdealer_id
subdealerid = 'subdealers2004'

api_Key ="ISNMdzuNiKG7jhl9d9v"

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

id_personnel = "create-personnel-s2004r1"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,id_personnel,account_type)

personnel_name = "personnel rep1"

#Required field . Used for
#SMS login in to the UI
#For Braango Enterprise, this is don't care
#unless partner implements UI with SMS login
sms_login = False


#This is a user name created while signing this personnel up.
#Typically this user name can be used to log into the braango UI.
#However for whitelabel product, it is expected that this will be used
#for single signon with respect to dealer account on partner system.
#i.e. it is expected that partner will pass on the same user name that
#dealer has on its system to give seamless integration experience.

user_name = "subdealers2004r1"

#Password will be encrypted with SHA-25 and base64 encoded and stored
#internally within the braango system. pattern:
#^(?=^.{6,10}$)(?=.*\d)(
#?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+}{&quot
#;:;'?/>.<,])(?!.*\s).*$
#Used for single sign on. needs to 6-10 characters with one capital,
#one numberal and one special character

password = "test1T$"

#Required
email = "s2004r1@subdealer2004r1.com"

#If email specified is that for
#CRM email (ADF XML compliant)
type_adf_crm = False


#Number where dealer's root account can be reached via SMS for leads
#pattern:^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$
phone_number = "4089763491"

#Number where dealer's root account can be reached via SMS for leads
#pattern:^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$
sms_number = "4089763491"

#Subscribe to the group
group = "s2004r1g"

#List of dealer banners. Braango will
#randomly choose one when sending
#message to dealer via SMS
dealer_banners = ["s2004r1db1"];


#List of client banners. Braango
#will randomly choose one when
#sending dealer messages to client
client_banners = ["s2004r1cb1"]


#List of dealer footers. Braango will
#randomly choose one when sending
#message to dealer via SMS
dealer_footers = ["s2004r1df1"];

#List of client footers. Braango
#will randomly choose one when
#sending dealer messages to client
client_footers = ["s2004r1cf1"];

#List of supervisor banners. Braango
#will randomly choose one when
#sending messages to supervisor
supervisor_banners = ["s2004r1sb1"];


#List of supervisor footers. Braango
#will randomly choose one when
#sending messages to supervisor
supervisor_footers = ["s2004r1sf1"];

# { sms_login, personnel_name, user_name, password, status, enabled, dealer_banners, client_banners, supervisor_banners, client_footers, dealer_footers,
# supervisor_footers, email, sms_number, phone_number, group, type_adf_crm }

personnelRequest = braango.models.PersonnelRequest(sms_login,personnel_name,user_name,password,None,None,dealer_banners,client_banners,
                                                   supervisor_banners,client_footers,dealer_footers,supervisor_footers,email,sms_number,phone_number,group,type_adf_crm)

# PersonnelRequestInput
personnelRequestInput = braango.PersonnelRequestInput(requestHeader,personnelRequest) 

try: 
    # Create personnel regular
    api_response = api_instance.create_personnel(subdealerid, body=personnelRequestInput)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PersonnelsApi->create_personnel: %s\n" % e)