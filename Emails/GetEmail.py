from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint

'''
  @author braango
  
    Sample code showing how to get emails for personnel
'''

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you

braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.EmailsApi()


# str | id of _sub_dealer_
subdealerid = 'subdealers2004' 

# str | if of _personnel_
salespersonid = 'c5313913-5768-4eef-b5ea-447cc8f5aec3'

# str | API Key to access this dealer's resources. 
#Value was returned when create_account api was called and 
#dealer was created first time 
api_key = 'ISNMdzuNiKG7jhl9d9v'

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts
account_type = "partner"

# bool | Specifies to get ADF CRM email records or plain email records.
#If not specified, default is false i.e. regular email ID accounts 
type_adf_crm = False 

try: 
    # Get email
    api_response = api_instance.get_email(subdealerid, salespersonid, api_key, account_type, type_adf_crm=type_adf_crm)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EmailsApi->get_email: %s\n" % e)