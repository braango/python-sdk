from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint

'''
  @author braango
         
          Sample code showing how to create new SMS number for personnel
'''
#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.SmsApi()

# str | id of _sub_dealer_
subdealerid = 'subdealers2004' 

# str | id of _personnel_
salespersonid = 'c5313913-5768-4eef-b5ea-447cc8f5aec3' 

api_Key ="ISNMdzuNiKG7jhl9d9v"

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

id_sms = "sms-create-s2004"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,id_sms,account_type)

sms_number = "4088723444"

smsInputBody = braango.models.SmsInputBody(sms_number)


# SmsInput | 
smsInput = braango.SmsInput(requestHeader,smsInputBody) 

try: 
    # Create SMS
    api_response = api_instance.create_sms(subdealerid, salespersonid, body=smsInput)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SmsApi->create_sms: %s\n" % e)
