import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint

''' 
  @author braango
         
          Sample code showing how to determine
          the phone line type
'''    

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.PhonesApi()

# str | provide number 
number = '6692459240' 

# str | API Key to access this dealer's resources. 
#Value was returned when create_account api was called and 
#dealer was created first time 
api_key = 'ISNMdzuNiKG7jhl9d9v'

# str | Dealer or partner is accessing this API
account_type = 'partner'
try: 
    # Get Line Type
    api_response = api_instance.get_line_type(number, api_key, account_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PhonesApi->get_line_type: %s\n" % e)
    
         
 
