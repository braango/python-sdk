from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint


''' 
  @author braango
         
          Sample code showing how to add supervisor
          for personnel for given group
         
          Note supervisor is another
          personnel within the system
         
          Both personnel i.e. supervisor
          and personnel to be supervised
          need to exist in the system
 '''
 
#TEST auth token. Please contact
#sales@braango.com to have one
#created for you  
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'


# create an instance of the API class
api_instance = braango.SupervisorsApi()

# str | id of _sub_dealer_
subdealerid = 'subdealers2004' 

# str | id of _personnel_
salespersonid = 'c5313913-5768-4eef-b5ea-447cc8f5aec3' 

 # str | 
group = "DEFAULT"

api_Key ="ISNMdzuNiKG7jhl9d9v"

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

id_supervisor = "group-supervisor-s2004"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,id_supervisor,account_type)

supervisor = "4fd2259e-10cb-46ce-b301-cf0b17677282"

supervisorInputBody = braango.models.SupervisorInputBody(supervisor)

# SupervisorInput |
supervisorInput = braango.SupervisorInput(requestHeader,supervisorInputBody) 

try: 
    # Create Supervisor
    api_response = api_instance.create_supervisor(subdealerid, salespersonid, group, body=supervisorInput)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SupervisorsApi->create_supervisor: %s\n" % e)      
 
