from __future__ import print_function
import time
import braango
import braango.models
import braango.apis
from braango.rest import ApiException
from pprint import pprint


''' 
  @author braango
         
          Sample code showing how to get all webhooks
'''

#TEST auth token. Please contact
#sales@braango.com to have one
#created for you
braango.configuration.api_key['auth_token'] = 'ISNWF0P30WM0CMK'

# create an instance of the API class
api_instance = braango.WebhooksApi()

# str | id of _sub_dealer_
subdealerid = 'subdealers2004' 

# str | id of _personnel_
salespersonid = 'c5313913-5768-4eef-b5ea-447cc8f5aec3' 

api_Key ="ISNMdzuNiKG7jhl9d9v"

#Set the account type to partner for
#virtual dealer and partner hosted
#accounts

account_type = "partner"

#This id for your tracking, will be reflected back in response.
#Doesn't get used by Braango
#If not speficied, braango will return session-id in its response

id_webhook = "sms-webhook-s2004"

# { "api_key": "ISNGvAzwuy4X7vAqrtV", "id": "any value",
#  "account_type": "partner" }
requestHeader = braango.models.RequestHeader(api_Key,id_webhook,account_type)

auth_id = "webhook-auth-id"

auth_key = "partner-webhook-auth-key"

enable = True

post_url = "https://test.partner.webhook/someresource"

webhookInputBody = braango.models.WebhookInputBody(enable, post_url, auth_key, auth_id)

# WebhookInput |  
webhookInput = braango.WebhookInput(requestHeader,webhookInputBody) 

try: 
    # Create Webhook
    api_response = api_instance.create_webhook(subdealerid, salespersonid, body=webhookInput)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebhooksApi->create_webhook: %s\n" % e)